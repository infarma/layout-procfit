package arquivoDePedido

import (
	"strconv"
	"strings"
)

type RegistroTipo1 struct {
	IdentificadorDoTipoDeRegistro  int64  `json:"IdentificadorDoTipoDeRegistro"`
	VersaoDoLayoutDoArquivo        string `json:"VersaoDoLayoutDoArquivo"`
	CnpjDoCliente                  string `json:"CnpjDoCliente"`
	CodigoDoClienteNaDistribuidora int32  `json:"CodigoDoClienteNaDistribuidora"`
	NumeroDoPedidoNoCliente        int64  `json:"NumeroDoPedidoNoCliente"`
	DataDoPedido                   int32  `json:"DataDoPedido"`
	HoraDoPedido                   int32  `json:"HoraDoPedido"`
}

func GetRegistroTipo1(runes []rune) RegistroTipo1 {
	registroTipo1 := RegistroTipo1{}

	identificador, _ := strconv.ParseInt(string(runes[0:2]), 10, 64)
	registroTipo1.IdentificadorDoTipoDeRegistro = int64(identificador)

	registroTipo1.VersaoDoLayoutDoArquivo = strings.TrimSpace(string(runes[2:8]))

	registroTipo1.CnpjDoCliente = string(runes[8:22])

	codigoDoClienteNaDistribuidora, _ := strconv.ParseInt(string(runes[22:28]), 10, 64)
	registroTipo1.CodigoDoClienteNaDistribuidora = int32(codigoDoClienteNaDistribuidora)

	numeroDoPedidoNoCliente, _ := strconv.ParseInt(string(runes[28:38]), 10, 64)
	registroTipo1.NumeroDoPedidoNoCliente = int64(numeroDoPedidoNoCliente)

	dataDoPedido, _ := strconv.ParseInt(string(runes[38:46]), 10, 64)
	registroTipo1.DataDoPedido = int32(dataDoPedido)

	horaDoPedido, _ := strconv.ParseInt(string(runes[46:50]), 10, 64)
	registroTipo1.HoraDoPedido = int32(horaDoPedido)

	return registroTipo1
}
