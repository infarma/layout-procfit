package arquivoDePedidoTest

import (
	"layout-procfit/v_105/arquivoDePedido"
	"os"
	"strconv"
	"testing"
)

func TestGetStruct(t *testing.T) {

	file, err := os.Open("../fileTest/00035101.PED")
	if err != nil {
		t.Error("não abriu arquivo", err)
	}

	arquivo := arquivoDePedido.GetStruct(file)

	//REGISTRO TIPO 1
	var registro1 arquivoDePedido.RegistroTipo1
	registro1.IdentificadorDoTipoDeRegistro = 01
	registro1.VersaoDoLayoutDoArquivo = "1.05"
	registro1.CnpjDoCliente = "15749976001887"
	registro1.CodigoDoClienteNaDistribuidora = 351
	registro1.NumeroDoPedidoNoCliente = 1
	registro1.DataDoPedido = 20141001
	registro1.HoraDoPedido = 1043

	if registro1.IdentificadorDoTipoDeRegistro != arquivo.RegistroTipo1.IdentificadorDoTipoDeRegistro {
		t.Error("Identificação do tipo e registro não é compativel")
	}

	if registro1.VersaoDoLayoutDoArquivo != arquivo.RegistroTipo1.VersaoDoLayoutDoArquivo {
		t.Error("Versão do layout não é compativel")
	}

	if registro1.CnpjDoCliente != arquivo.RegistroTipo1.CnpjDoCliente {
		t.Error("Cnpj do cliente não é compativel")
	}

	if registro1.CodigoDoClienteNaDistribuidora != arquivo.RegistroTipo1.CodigoDoClienteNaDistribuidora {
		t.Error("Cogigo do cliente na distribuidoranão é compativel")
	}

	if registro1.NumeroDoPedidoNoCliente != arquivo.RegistroTipo1.NumeroDoPedidoNoCliente {
		t.Error("Numero do pedido no cliente não é compativel")
	}

	if registro1.DataDoPedido != arquivo.RegistroTipo1.DataDoPedido {
		t.Error("Data do pedido não é compativel")
	}

	if registro1.HoraDoPedido != arquivo.RegistroTipo1.HoraDoPedido {
		t.Error("Hora do pedido não é compativel")
	}

	//REGISTRO TIPO 2
	var registro2 arquivoDePedido.RegistroTipo2
	registro2.IdentificadorDoTipoDeRegistro = 02
	registro2.Mensagem = ""

	if registro2.IdentificadorDoTipoDeRegistro != arquivo.RegistroTipo2[0].IdentificadorDoTipoDeRegistro {
		t.Error("Identificador do tipo de registro não é compativel")
	}

	if registro2.Mensagem != arquivo.RegistroTipo2[0].Mensagem {
		t.Error(" Mensagem não é compativel")
	}

	//REGISTRO TIPO 3
	codigoDoProduto, _ := strconv.ParseInt("7896714203690", 10, 64)
	quatidade,  _ := strconv.ParseInt("1", 10, 32)

	var registro3  arquivoDePedido.RegistroTipo3
	registro3.IdentificadorDoTipoDeRegistro   = 03
	registro3.TipoDeCodigoDoProduto = 2
	registro3.CodigoDoProduto = string(codigoDoProduto)
	registro3.Quantidade = int32(quatidade)

	if registro3.IdentificadorDoTipoDeRegistro != arquivo.RegistroTipo3[0].IdentificadorDoTipoDeRegistro {
		t.Error("Identificador do tipo de registro  não é compativel")
	}

	if registro3.TipoDeCodigoDoProduto != arquivo.RegistroTipo3[0].TipoDeCodigoDoProduto {
		t.Error("Tipo o codigo do produto não é compativel")
	}

	if registro3.CodigoDoProduto != arquivo.RegistroTipo3[0].CodigoDoProduto {
		t.Error("Codigo do produto não é compativel:", registro3.CodigoDoProduto, " != " , arquivo.RegistroTipo3[0].CodigoDoProduto )
	}

	if registro3.Quantidade != arquivo.RegistroTipo3[0].Quantidade {
		t.Error("Quantidade não é compativel:", registro3.Quantidade, " != ", arquivo.RegistroTipo3[0].Quantidade )
	}

	//REGISTRO TIPO 9
	identificadorDoTipoDeRegistro, _ := strconv.ParseInt("09", 10, 32)

	var registro9 arquivoDePedido.RegistroTipo9
	registro9.IdentificadorDoTipoDeRegistro = int32(identificadorDoTipoDeRegistro)
	registro9.QuantidadeTotalDeItens = 13
	registro9.QuantidadeTotalDeUnidades  =  42

	if registro9.IdentificadorDoTipoDeRegistro != arquivo.RegistroTipo9.IdentificadorDoTipoDeRegistro {
		t.Error("Identificador do tipo de registro não é compativel")
	}

	if registro9.QuantidadeTotalDeItens != arquivo.RegistroTipo9.QuantidadeTotalDeItens {
		t.Error("Quantidade não é compativel")
	}

	if registro9.QuantidadeTotalDeUnidades != arquivo.RegistroTipo9.QuantidadeTotalDeUnidades {
		t.Error("Quantidade total de unidades não é compativel")
	}
}
