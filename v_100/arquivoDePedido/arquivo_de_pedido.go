package arquivoDePedido

import (
	"bufio"
	"os"
)

// Arquivo de Pedido
type ArquivoDePedido struct {
	RegistroTipo1 RegistroTipo1   `json:"RegistroTipo1"`
	RegistroTipo2 []RegistroTipo2 `json:"RegistroTipo2"`
	RegistroTipo3 []RegistroTipo3 `json:"RegistroTipo3"`
	RegistroTipo9 RegistroTipo9   `json:"RegistroTipo9"`
}

func GetStruct(fileHandle *os.File) ArquivoDePedido {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:2])

		if identificador == "01" {
			arquivo.RegistroTipo1 = GetRegistroTipo1(runes)
		} else if identificador == "02" {
			arquivo.RegistroTipo2 = append(arquivo.RegistroTipo2, GetRegistroTipo2(runes))
		} else if identificador == "03" {
			arquivo.RegistroTipo3 = append(arquivo.RegistroTipo3, GetRegistroTipo3(runes))
		} else if identificador == "09" {
			arquivo.RegistroTipo9 = GetRegistroTipo9(runes)
		}
	}
	return arquivo
}
