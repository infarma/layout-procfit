package arquivoDeRetorno

import (
	"fmt"
	"strconv"
)

// Arquivo de Retorno
type ArquivoDePedido struct {
	RegistroTipo1 RegistroTipo1   `json:"RegistroTipo1"`
	RegistroTipo2 []RegistroTipo2 `json:"RegistroTipo2"`
	RegistroTipo9 RegistroTipo9   `json:"RegistroTipo9"`
}

// RegistroTipo1
func HeaderArquivo(numCnpj string, codPedCli string, numPedVen int32) string {
	cabecalhoArquivo := fmt.Sprint("01")
	cabecalhoArquivo += fmt.Sprint("1.00  ")
	cabecalhoArquivo += fmt.Sprintf("%014d", ConvertStringToInt(numCnpj))
	cabecalhoArquivo += fmt.Sprintf("%010d", ConvertStringToInt(codPedCli))
	cabecalhoArquivo += fmt.Sprintf("%010d", numPedVen)
	return cabecalhoArquivo
}

// RegistroTipo2
func ItensArquivo(tipCodPrd string, codProdut string, codPrdCli int32, qtdAtendi int32, codMtvRejIte string) string {
	itensArquivo := fmt.Sprint("02")

	if tipCodPrd == "1" {
		itensArquivo += fmt.Sprint("1")
		itensArquivo += fmt.Sprintf("%013d", codPrdCli)
	} else {
		itensArquivo += fmt.Sprint("2")
		itensArquivo += fmt.Sprintf("%-13d", ConvertStringToInt(codProdut))
	}
	itensArquivo += fmt.Sprintf("%09d", qtdAtendi*100)

	itensArquivo += fmt.Sprintf("%03d", ConvertStringToInt(codMtvRejIte))
	return itensArquivo
}

// RegistroTipo9
func RodapeArquivo(qtdIte int32, qtdUnd int32) string {
	rodape := fmt.Sprint("09")
	rodape += fmt.Sprintf("%04d", qtdIte)
	rodape += fmt.Sprintf("%010d", qtdUnd*100)
	return rodape
}

func ConvertStringToInt(valor string) int {
	v, _ := strconv.Atoi(valor)
	return v
}