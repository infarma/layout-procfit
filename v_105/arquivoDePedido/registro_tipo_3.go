package arquivoDePedido

import "strconv"

type RegistroTipo3 struct {
	IdentificadorDoTipoDeRegistro int32  `json:"IdentificadorDoTipoDeRegistro"`
	TipoDeCodigoDoProduto         int32  `json:"TipoDeCodigoDoProduto"`
	CodigoDoProduto               string `json:"CodigoDoProduto"`
	Quantidade                    int32  `json:"Quantidade"`
}

func GetRegistroTipo3(runes []rune) RegistroTipo3 {
	registroTipo3 := RegistroTipo3{}

	identificador, _ := strconv.ParseInt(string(runes[0:2]), 10, 64)
	registroTipo3.IdentificadorDoTipoDeRegistro = int32(identificador)

	tipoDeCodigoDoProduto, _ := strconv.ParseInt(string(runes[2:3]), 10, 64)
	registroTipo3.TipoDeCodigoDoProduto = int32(tipoDeCodigoDoProduto)

	codigoInterno, _ := strconv.ParseInt(string(runes[3:16]), 10, 64)
	registroTipo3.CodigoDoProduto = string(codigoInterno)

	quantidade, _ := strconv.ParseInt(string(runes[16:23]), 10, 64)
	registroTipo3.Quantidade = int32(quantidade)

	return registroTipo3
}
