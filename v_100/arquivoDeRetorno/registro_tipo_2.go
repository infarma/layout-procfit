package arquivoDeRetorno

import "strconv"

type RegistroTipo2 struct {
	IdentificadorDoTipoDeRegistro int64   `json:"IdentificadorDoTipoDeRegistro"`
	TipoDeCodigoDoProduto         int64   `json:"TipoDeCodigoDoProduto"`
	CodigoDoProduto               int64   `json:"CodigoDoProduto"`
	QuantidadeDoProdutoAtendida   float64 `json:"QuantidadeDoProdutoAtendida"`
	MotivoNaoAtendimento          string  `json:"MotivoNaoAtendimento"`
}

func GetRegistroTipo2(runes []rune) RegistroTipo2 {
	RegistroTipo2 := RegistroTipo2{}

	identificador, _ := strconv.ParseInt(string(runes[0:2]), 10, 64)
	RegistroTipo2.IdentificadorDoTipoDeRegistro = int64(identificador)

	tipoDeCodigoDoProduto, _ := strconv.ParseInt(string(runes[2:3]), 10, 64)
	RegistroTipo2.TipoDeCodigoDoProduto = int64(tipoDeCodigoDoProduto)

	codigoDoProduto, _ := strconv.ParseInt(string(runes[3:16]), 10, 64)
	RegistroTipo2.CodigoDoProduto = int64(codigoDoProduto)

	quantidadeDoProdutoAtendida, _ := strconv.ParseInt(string(runes[16:23]), 10, 64)
	RegistroTipo2.QuantidadeDoProdutoAtendida = float64(quantidadeDoProdutoAtendida)

	RegistroTipo2.MotivoNaoAtendimento = string(runes[23:25])

	return RegistroTipo2
}
