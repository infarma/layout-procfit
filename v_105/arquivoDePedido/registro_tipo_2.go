package arquivoDePedido

import (
	"strconv"
	"strings"
)

type RegistroTipo2 struct {
	IdentificadorDoTipoDeRegistro int32  `json:"IdentificadorDoTipoDeRegistro"`
	Mensagem                      string `json:"Mensagem"`
}

func GetRegistroTipo2(runes []rune) RegistroTipo2 {
	registroTipo2 := RegistroTipo2{}

	identificador, _ := strconv.ParseInt(string(runes[0:2]), 10, 64)
	registroTipo2.IdentificadorDoTipoDeRegistro = int32(identificador)

	registroTipo2.Mensagem = strings.TrimSpace(string(runes[2:42]))

	return registroTipo2
}
