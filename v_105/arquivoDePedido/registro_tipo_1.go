package arquivoDePedido

import (
	"strconv"
	"strings"
)

type RegistroTipo1 struct {
	IdentificadorDoTipoDeRegistro  int32  `json:"IdentificadorDoTipoDeRegistro"`
	VersaoDoLayoutDoArquivo        string `json:"IdentificadorDoTipoDeRegistro"`
	CnpjDoCliente                  string `json:"CnpjDoCliente"`
	CodigoDoClienteNaDistribuidora int32  `json:"CodigoDoClienteNaDistribuidora"`
	NumeroDoPedidoNoCliente        int32  `json:"NumeroDoPedidoNoCliente"`
	DataDoPedido                   int32  `json:"DataDoPedido"`
	HoraDoPedido                   int32  `json:"HoraDoPedido"`
}

func GetRegistroTipo1(runes []rune) RegistroTipo1 {
	registroTipo1 := RegistroTipo1{}

	identificador, _ := strconv.ParseInt(string(runes[0:2]), 10, 64)
	registroTipo1.IdentificadorDoTipoDeRegistro = int32(identificador)

	registroTipo1.VersaoDoLayoutDoArquivo = strings.TrimSpace(string(runes[2:8]))

	registroTipo1.CnpjDoCliente = string(runes[8:22])

	codigoDoClienteNaDistribuidora, _ := strconv.ParseInt(string(runes[22:31]), 10, 64)
	registroTipo1.CodigoDoClienteNaDistribuidora = int32(codigoDoClienteNaDistribuidora)

	numeroDoPedidoNoCliente, _ := strconv.ParseInt(string(runes[31:40]), 10, 64)
	registroTipo1.NumeroDoPedidoNoCliente = int32(numeroDoPedidoNoCliente)

	dataDoPedido, _ := strconv.ParseInt(string(runes[40:48]), 10, 64)
	registroTipo1.DataDoPedido = int32(dataDoPedido)

	horaDoPedido, _ := strconv.ParseInt(string(runes[48:52]), 10, 64)
	registroTipo1.HoraDoPedido = int32(horaDoPedido)

	return registroTipo1
}
