package arquivoDeRetorno

import "strconv"

type RegistroTipo9 struct {
	IdentificadorDoTipoDeRegistro int32 `json:"IdentificadorDoTipoDeRegistro"`
	QuantidadeTotalDeItens        int32 `json:"QuantidadeTotalDeItens"`
	QuantidadeTotalDeUnidades     int32 `json:"QuantidadeTotalDeUnidades"`
}

func GetRegistroTipo9(runes []rune) RegistroTipo9 {
	RegistroTipo9 := RegistroTipo9{}

	identificador, _ := strconv.ParseInt(string(runes[0:2]), 10, 64)
	RegistroTipo9.IdentificadorDoTipoDeRegistro = int32(identificador)

	tipoDeCodigoDoProduto, _ := strconv.ParseInt(string(runes[2:6]), 10, 64)
	RegistroTipo9.QuantidadeTotalDeItens = int32(tipoDeCodigoDoProduto)

	quantidadeTotalDeUnidades, _ := strconv.ParseInt(string(runes[6:14]), 10, 64)
	RegistroTipo9.QuantidadeTotalDeUnidades = int32(quantidadeTotalDeUnidades)

	return RegistroTipo9
}
