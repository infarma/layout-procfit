package arquivoDeRetorno

import "strconv"

type RegistroTipo1 struct {
	IdentificadorDoTipoDeRegistro  int32  `json:"IdentificadorDoTipoDeRegistro"`
	VersaoDoLayoutDoArquivo        string `json:"VersaoDoLayoutDoArquivo"`
	CnpjDoCliente                  string `json:"CnpjDoCliente"`
	NumeroDoPedidoNoCliente        int32  `json:"NumeroDoPedidoNoCliente"`
	NumeroDoPedidoNoDistribuidor   int32  `json:"NumeroDoPedidoNoDistribuidor"`
	MotivoDoNaoAtendimentoDoPedido string `json:"MotivoDoNaoAtendimentoDoPedido"`
}

func GetRegistroTipo1(runes []rune) RegistroTipo1 {
	RegistroTipo1 := RegistroTipo1{}

	identificador, _ := strconv.ParseInt(string(runes[0:2]), 10, 64)
	RegistroTipo1.IdentificadorDoTipoDeRegistro = int32(identificador)

	RegistroTipo1.VersaoDoLayoutDoArquivo = string(runes[2:8])

	RegistroTipo1.CnpjDoCliente = string(runes[8:22])

	numeroDoPedidoNoCliente, _ := strconv.ParseInt(string(runes[22:31]), 10, 64)
	RegistroTipo1.NumeroDoPedidoNoCliente = int32(numeroDoPedidoNoCliente)

	numeroDoPedidoNoDistribuidor, _ := strconv.ParseInt(string(runes[31:41]), 10, 64)
	RegistroTipo1.NumeroDoPedidoNoDistribuidor = int32(numeroDoPedidoNoDistribuidor)

	RegistroTipo1.MotivoDoNaoAtendimentoDoPedido = string(runes[41:43])

	return RegistroTipo1
}
