package layout_procfit

import (
	v100 "bitbucket.org/infarma/layout-procfit/v_100/arquivoDePedido"
	v105 "bitbucket.org/infarma/layout-procfit/v_105/arquivoDePedido"
	"os"
)

func GetArquivoDePedidoV100(fileHandle *os.File) v100.ArquivoDePedido {
	return v100.GetStruct(fileHandle)
}

func GetArquivoDePedidoV105(fileHandle *os.File) v105.ArquivoDePedido {
	return v105.GetStruct(fileHandle)
}
