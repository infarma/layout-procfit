package layout_procfit

import (
	layout_procfit "bitbucket.org/infarma/layout-procfit"
	"os"
	"testing"
)

func TestGetArquivoDePedidoV100(t *testing.T){
	f, err := os.Open("test/fileTest/00035101.PED")

	if err != nil {
		t.Error("Erro ao abrir o arquivo modelo")
	}

	arq := layout_procfit.GetArquivoDePedidoV100(f)

	if arq.RegistroTipo1.CnpjDoCliente == "" {
		t.Errorf("CNPJ não foi lido %s", arq.RegistroTipo1.CnpjDoCliente)
	} else {
		t.Logf("CNPJ lido: %s", arq.RegistroTipo1.CnpjDoCliente)
	}
}


func TestGetArquivoDePedidoV105(t *testing.T){
	f, err := os.Open("test/v_105_test/fileTest/00035101.PED")

	if err != nil {
		t.Error("Erro ao abrir o arquivo modelo")
	}

	arq := layout_procfit.GetArquivoDePedidoV105(f)

	if arq.RegistroTipo1.CnpjDoCliente == "" {
		t.Errorf("CNPJ não foi lido %s", arq.RegistroTipo1.CnpjDoCliente)
	} else {
		t.Logf("CNPJ lido: %s", arq.RegistroTipo1.CnpjDoCliente)
	}
}