package arquivoDePedido

import "strconv"

type RegistroTipo9 struct {
	IdentificadorDoTipoDeRegistro int64   `json:"IdentificadorDoTipoDeRegistro"`
	QuantidadeTotalDeItens        int64   `json:"QuantidadeTotalDeItens"`
	QuantidadeTotalDeUnidades     float64 `json:"QuantidadeTotalDeUnidades"`
}

func GetRegistroTipo9(runes []rune) RegistroTipo9 {
	registroTipo9 := RegistroTipo9{}

	identificador, _ := strconv.ParseInt(string(runes[0:2]), 10, 64)
	registroTipo9.IdentificadorDoTipoDeRegistro = int64(identificador)

	quantidadeTotalDeItens, _ := strconv.ParseInt(string(runes[2:6]), 10, 64)
	registroTipo9.QuantidadeTotalDeItens = int64(quantidadeTotalDeItens)

	quantidadeTotalDeUnidades, _ := strconv.ParseInt(string(runes[6:14]), 10, 64)
	registroTipo9.QuantidadeTotalDeUnidades = float64(quantidadeTotalDeUnidades)

	return registroTipo9
}
