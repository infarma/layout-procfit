package arquivoDeRetorno

import (
	"layout-procfit/v_100/arquivoDeRetorno"
	"testing"
)

func TestHeaderArquivo(t *testing.T){
	numCnpj := "15749976001887"
	codPedCli := "351"
	numPedVen := 1

	header := arquivoDeRetorno.HeaderArquivo(numCnpj, codPedCli, int32(numPedVen)) //entrar com os dados do banco

	if (len(header) != 42) {
		t.Error("Cabecalho não tem o tamanho adequado")
	}else{
		if header != "011.00  1574997600188700000003510000000001" {
			t.Error("Cabecalho não é compativel")
		}
	}
}

func TestItensArquivo(t *testing.T){
	tipCodPrd := "2"
	codProdut := "7896004715698"
	codPrdCli := 1
	qtdAtendi := 1
	codMtvRejIte := "000"

	header := arquivoDeRetorno.ItensArquivo(tipCodPrd, codProdut, int32(codPrdCli), int32(qtdAtendi), codMtvRejIte) //entrar com os dados do banco
	if len(header) != 28 {
		t.Error("Cabecalho não tem o tamanho adequado")
	}else{
		if header != "0227896004715698000000100000" {
			t.Error("Cabecalho não é compativel")
		}
	}
}

func TestRodapeArquivo(t *testing.T){
	qtdIte := 3
	qtdUnd := 7

	header := arquivoDeRetorno.RodapeArquivo(int32(qtdIte), int32(qtdUnd)) //entrar com os dados do banco
	if len(header) != 16 {
		t.Error("Cabecalho não tem o tamanho adequado")
	}else{
		if header != "0900030000000700" {
			t.Error("Cabecalho não é compativel")
		}
	}
}
