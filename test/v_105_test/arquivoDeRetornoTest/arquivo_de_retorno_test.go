package arquivoDeRetornoTest

import (
	"layout-procfit/v_105/arquivoDeRetorno"
	"testing"
)

func TestHeaderArquivo(t *testing.T){
	numCnpj := "15749976001887"
	codPedCli := "351"
	numPedVen := 1
	codMtvRejCab := "2"

	header := arquivoDeRetorno.HeaderArquivo(numCnpj, codPedCli, int32(numPedVen), codMtvRejCab) //entrar com os dados do banco

	if (len(header) != 43) && (len(header) != 42) {
		t.Error("Cabecalho não tem o tamanho adequado")
	}else{
		if header != "011.03  1574997600188700000035100000000012 " {
			t.Error("Cabecalho não é compativel")
		}
	}
}

func TestItensArquivo(t *testing.T){
	tipCodPrd := "2"
	codProdut := "7896004715698"
	codPrdCli := 1
	qtdAtendi := 1
	codMtvRejCab := "00"

	header := arquivoDeRetorno.ItensArquivo(tipCodPrd, codProdut, int32(codPrdCli), int32(qtdAtendi), codMtvRejCab) //entrar com os dados do banco
	if len(header) != 25 {
		t.Error("Cabecalho não tem o tamanho adequado")
	}else{
		if header != "0227896004715698000000100" {
			t.Error("Cabecalho não é compativel")
		}
	}
}

func TestRodapeArquivo(t *testing.T){
	qtdIte := 3
	qtdUnd := 7

	header := arquivoDeRetorno.RodapeArquivo(int32(qtdIte), int32(qtdUnd)) //entrar com os dados do banco
	if len(header) != 14 {
		t.Error("Cabecalho não tem o tamanho adequado")
	}else{
		if header != "09000300000007" {
			t.Error("Cabecalho não é compativel")
		}
	}
}